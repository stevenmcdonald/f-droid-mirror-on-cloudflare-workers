export class CacheKey {
  value: URL

  constructor(url: URL, language: string) {
    this.value = new URL(url.toString())
    this.value.searchParams.append('lang', language)
  }

  toString(): string {
    return this.value.toString()
  }
}
